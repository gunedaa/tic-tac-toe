#ifndef _TICTACTOENCURSES_H
#define _TICTACTOENCURSES_H

#include <stdint.h>
#include <stdio.h>
#include <unistd.h>
#include <ncurses.h>
#include <curses.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>

const int INPUT_X = 1;
const int INPUT_O = 0;

typedef enum {
    MY_TURN = 1,
    YOUR_TURN = 0
}Turn;

int boardData[9]; //store the values for each square
Turn turn;
char input; // store player input
int nrOfMoves = 0; // store the nr of moves (9 at most)
int winsOfX = 0;  // store the wins of X
int winsOfO = 0; // store the wins of O
int nrOfTieGames = 0; // store the nr of times the game was a tie
char inputToBeSent; //the input of the player to be sent to the other player
char inputToBeReceived; //the input of the other player to be received

//Functions
void initBoard(int * boardData);
void DrawTicTacToeBoard();
bool CheckValidity(int * boardData, int place, int XorO);
bool CheckWin(int * boardData);
void initNcurses();
void CheckTie();
char SendInput();
int UpdateTheBoard(int x, int y, int inputPlace, int currentPlayer);
void PlayTicTacToePlayer1(char inputToBeGotten, char inputToBeProvided);
void PlayTicTacToePlayer2(char inputToBeGotten, char inputToBeProvided);


#endif
 