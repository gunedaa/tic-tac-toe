/* 
   This code is written to create a Tic Tac Toe game:
    * creates a GUI by makig use of Ncurses
    * checks if the user input is valid
    * checks if the last move was a win
    * checks if the game is a tie
    * keeps track of the scores of matches
*/

#include "TicTacToeNcurses.h"

/* Initialize the board with random numbers
    *random is necessary due to CheckWin()
*/
void initBoard(int * boardData){
    int random = 5;
	for(int i=0; i<=8;i++){
		boardData[i] = random++;
	}
}
void DrawTicTacToeBoard(){
    //first vertical line
    for(int i = 3; i<20; i++){
        mvprintw(i, 14,"|");
    }
    //second vertical line
    for(int i = 3; i<20; i++){
        mvprintw(i, 24,"|");
    }
    //first horizontal line
    mvprintw(8, 5,"_______________________________");
    //second horizontal line
    mvprintw(14, 5,"_______________________________");

}
/* Check if the entered input is valid , if yes record the input */
bool CheckValidity(int * boardData, int place, int XorO){
    place--; //to adjust the places for the array
    if(boardData[place] > 1 ){ //there is no input in this place
        boardData[place] = XorO; //record the input
        return true; 
    }
    else
    {
        mvprintw(1, 9,"Invalid input, try again!");
        refresh();
        usleep(1000000); //sleep
        mvprintw(1, 9,"                          ");
        refresh();
        return false; //there is already an input here
    }
}
bool CheckWin(int * boardData){
    /* Check rows */
	if( boardData[0] == boardData[1] && boardData[1] == boardData[2] )
		return true;
	else if( boardData[3] == boardData[4] && boardData[4] == boardData[5] )
		return true;	
	else if( boardData[6] == boardData[7] && boardData[7] == boardData[8] )
		return true;

	/* Check columns */
	else if( boardData[0] == boardData[3] && boardData[3] == boardData[6] )
		return true;
	else if( boardData[1] == boardData[4] && boardData[4] == boardData[7] )
		return true;
	else if( boardData[2] == boardData[5] && boardData[5] == boardData[8] )
		return true;

	/* Check diagonals */
	else if( boardData[0] == boardData[4] && boardData[4] == boardData[8] )
		return true;
	else if( boardData[2] == boardData[4] && boardData[4] == boardData[6] )
		return true;
    else
        return false;
}
void initNcurses(){
    initscr();              /* Start curses mode */
    noecho();               /* no cursor */
    curs_set(0);			/* no cursor */
    cbreak();
    keypad(stdscr, TRUE);
    mvprintw(11, 9,"Welcome to tic tac toe!");
    mvprintw(13, 17,"Enjoy!");
    mvprintw(15, 18,"^-^");
    refresh(); //refresh the window after print
    usleep(2000000); //sleep
    erase(); //clear the window
}
void CheckTie(){
    if(nrOfMoves == 9){
        mvprintw(1, 9,"Tie game!");
        refresh();
        usleep(2000000); //sleep
        mvprintw(1, 9,"          ");
        refresh();
        erase();
        initBoard(boardData);
        nrOfTieGames++;
        nrOfMoves = 0;
        DrawTicTacToeBoard();
        inputToBeSent = 't';
    }
}
int UpdateTheBoard(int x, int y, int inputPlace, int currentPlayer){
    switch (currentPlayer)
    {
    case 1:
        if(CheckValidity(boardData, inputPlace, currentPlayer)){
            mvprintw(x, y, "X");
            refresh();
            nrOfMoves++;
            inputToBeSent = inputPlace + '0';
            mvprintw(3, 38,"Nr of moves: %d", nrOfMoves);
            refresh();
            if(CheckWin(boardData))
            {
                winsOfX++;
                nrOfMoves = 0;
                inputToBeSent = 'w';
                initBoard(boardData);
                mvprintw(1, 9,"Player X won!");
                refresh();
                usleep(2000000);
                erase();
                DrawTicTacToeBoard();
            }
            else{
                CheckTie();
            }
        }
        break;
        return 1;
    case 0:
        if(CheckValidity(boardData, inputPlace, currentPlayer)){
            mvprintw(x, y, "O");
            refresh();
            nrOfMoves++;
            inputToBeSent = inputPlace +'0';
            mvprintw(3, 38,"Nr of moves: %d", nrOfMoves);
            refresh();
            if(CheckWin(boardData))
            {
                winsOfO++;
                nrOfMoves = 0;
                inputToBeSent = 'w';
                initBoard(boardData);
                mvprintw(1, 9,"Player O won!");
                refresh();
                usleep(2000000);
                erase();
                DrawTicTacToeBoard();
            }
            else{
                CheckTie();
            }
        }
        break;
        return 1;
    default:
        break;
        return 1;
    }
}
char SendInput(){
    return inputToBeSent;
}
/* Play the game by initializing the player nr 
    @playerNr = 1 is assigned X
    @playerNr = 2 is assigned O
    => players play one by one,
       i.e. waits until an input is received from the other player
*/