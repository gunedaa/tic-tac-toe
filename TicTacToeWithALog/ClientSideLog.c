#include "ClientSideLog.h"


#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/fcntl.h>
#include <stdlib.h>
#include <getopt.h>

#define SHM_SIZE 100
#define MAX_DATA 80
static int  shm_fd = -1;
char *shm_name = "log";
char *shm_addr;

//creates the shared memory
static char * my_shm_create (char * shm_name, int size)
{
    int     rtnval;
    char *  shm_addr;
    
    printf ("Calling shm_open('%s')\n", shm_name);
    // creating the acctual memory ()
    shm_fd = shm_open (shm_name, O_CREAT | O_EXCL | O_RDWR, 0600);
    //by deafult the shm_fd is -1, so if it didn´t change something went wrong
    if (shm_fd == -1)
    {
        perror ("ERROR: shm_open() failed");
    }
    printf ("shm_open() returned %d\n", shm_fd);
                
    printf ("Calling ftrucate(%d,%d)\n", shm_fd, size);
    //expanding the memory with null values 
    rtnval = ftruncate (shm_fd, size);
    if (rtnval != 0)
    {
        perror ("ERROR: ftruncate() failed");
    }
    printf ("ftruncate() returned %d\n", rtnval);
                
    printf ("Calling mmap(len=%d,fd=%d)\n", size, shm_fd);
    //creates a mirror/map of the file in memory
    //map is only a working file 
    shm_addr = (char *) mmap (NULL, size, PROT_READ | PROT_WRITE, MAP_SHARED, shm_fd, 0);
    if (shm_addr == MAP_FAILED)
    {
        perror ("ERROR: mmap() failed");
    }
    printf ("mmap() returned %p\n", shm_addr);

    return (shm_addr);
}
//open shared memory by passing a name
static char * my_shm_open (char * shm_name)
{
    int     size;
    char *  shm_addr;
    
    printf ("Calling shm_open('%s')\n", shm_name);
    //opens the memory file with the matching name
    shm_fd = shm_open (shm_name, O_RDWR, 0600); // O_RDWR -> open read write (mode)
    if (shm_fd == -1)
    {
        perror ("ERROR: shm_open() failed");
    }
    printf ("shm_open() returned %d\n", shm_fd);
    printf ("Calling lseek(fd=%d,SEEK_END)\n", shm_fd);
    //size gets the value from the file size by using lseek which doesnt change the offset but merly measures it in this case
    size = lseek (shm_fd, 0, SEEK_END);
    printf ("lseek() returned %d\n", size);
    printf ("Calling mmap(len=%d,fd=%d)\n", size, shm_fd);
    //opening the file
    shm_addr = (char *) mmap (NULL, size, PROT_READ | PROT_WRITE, MAP_SHARED, shm_fd, 0);
    if (shm_addr == MAP_FAILED)
    {
        perror ("ERROR: mmap() failed");
    }
    printf ("mmap() returned %p\n", shm_addr);

    return (shm_addr);
}

int closure(const char* name, const size_t size, void *addr){

    int ret;

    ret = munmap(addr, size);
    if(ret == -1){
    perror("munmap");
    }

}
void writeData(void* addr, char* result){
    char buf;
    for(int i = 0; i <= sizeof(result); i++){
        buf = result[i]; //received from the client
        memcpy(addr, &buf, 1);
    }
    printf("Updated Shared Memory!\n");
    closure("Shared Memory closed!\n", SHM_SIZE, shm_addr);

}