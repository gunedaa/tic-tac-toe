#include <stdio.h>
#include <stdlib.h>     // for atoi() and exit()
#include <string.h>     // for strlen()
#include <arpa/inet.h>  // for sockaddr_in and inet_ntoa()
#include <pthread.h>    // for POSIX threads
#include <sys/socket.h> // for socket(), bind(), getsockname and connect()
#include <time.h>       // for time()
#include <unistd.h>     // for sleep(), close()
#include <errno.h>
//For share memory
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/fcntl.h>
#include <getopt.h>

#include "TicTacToeNcurses.h"

#define SHM_SIZE 100 /* Size of shared memory */
#define TIE "THE MATCH IS A TIE!\n"
#define WIN_O "O WON!\n"
#define WIN_X "X WON!\n"

int CreateTCPClientSocket(const char *servIP, unsigned short port);
unsigned char receiveMessage(int sock);
void sendMessage(int sock, char *message);
/* Shared Memory */
static int shm_fd = -1;
char *shm_name = "log";
char *shm_addr;
//open shared memory by passing a name
static char *my_shm_open(char *shm_name)
{
    int size;
    char *shm_addr;

    shm_fd = shm_open(shm_name, O_RDWR, 0600); // O_RDWR -> open read write (mode)
    if (shm_fd == -1)
    {
        perror("ERROR: shm_open() failed");
    }

    size = lseek(shm_fd, 0, SEEK_END);

    shm_addr = (char *)mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_SHARED, shm_fd, 0);
    if (shm_addr == MAP_FAILED)
    {
        perror("ERROR: mmap() failed");
    }

    return (shm_addr);
}

int closure(const char *name, const size_t size, void *addr)
{
    int ret;
    ret = munmap(addr, size);
    if (ret == -1)
    {
        perror("munmap");
    }
}
void WriteDataTie()
{
    shm_addr = my_shm_open(shm_name);
    sprintf(shm_addr, TIE);
    mvprintw(2, 9, "Updated shared memory!");
    refresh();
    usleep(20000);
    closure("Shared Memory closed!\n", SHM_SIZE, shm_addr);
}
void WriteDataX()
{
    shm_addr = my_shm_open(shm_name);
    sprintf(shm_addr, WIN_X);
    mvprintw(2, 9, "Updated shared memory!");
    refresh();
    usleep(20000);
    closure("Shared Memory closed!\n", SHM_SIZE, shm_addr);
}
void WriteDataO()
{
    shm_addr = my_shm_open(shm_name);
    sprintf(shm_addr, WIN_O);
    mvprintw(1, 9, "Updated shared memory!");
    refresh();
    usleep(20000);
    closure("Shared Memory closed!\n", SHM_SIZE, shm_addr);
}
/* Initialize the board with random numbers
    *random is necessary due to CheckWin()*/
void initBoard(int *boardData)
{
    int random = 5;
    for (int i = 0; i <= 8; i++)
    {
        boardData[i] = random++;
    }
}
void DrawTicTacToeBoard()
{
    //first vertical line
    for (int i = 3; i < 20; i++)
    {
        mvprintw(i, 14, "|");
    }
    //second vertical line
    for (int i = 3; i < 20; i++)
    {
        mvprintw(i, 24, "|");
    }
    //first horizontal line
    mvprintw(8, 5, "_______________________________");
    //second horizontal line
    mvprintw(14, 5, "_______________________________");
}
/* Check if the entered input is valid , if yes record the input */
bool CheckValidity(int *boardData, int place, int XorO)
{
    place--; //to adjust the places for the array
    if (boardData[place] > 1)
    {                            //there is no input in this place
        boardData[place] = XorO; //record the input
        return true;
    }
    else
    {
        mvprintw(1, 9, "Invalid input, try again!");
        refresh();
        usleep(100000); //sleep
        mvprintw(1, 9, "                          ");
        refresh();
        return false; //there is already an input here
    }
}
bool CheckWin(int *boardData)
{
    /* Check rows */
    if (boardData[0] == boardData[1] && boardData[1] == boardData[2])
        return true;
    else if (boardData[3] == boardData[4] && boardData[4] == boardData[5])
        return true;
    else if (boardData[6] == boardData[7] && boardData[7] == boardData[8])
        return true;

    /* Check columns */
    else if (boardData[0] == boardData[3] && boardData[3] == boardData[6])
        return true;
    else if (boardData[1] == boardData[4] && boardData[4] == boardData[7])
        return true;
    else if (boardData[2] == boardData[5] && boardData[5] == boardData[8])
        return true;

    /* Check diagonals */
    else if (boardData[0] == boardData[4] && boardData[4] == boardData[8])
        return true;
    else if (boardData[2] == boardData[4] && boardData[4] == boardData[6])
        return true;
    else
        return false;
}
void initNcurses()
{
    initscr();   /* Start curses mode */
    noecho();    /* no cursor */
    curs_set(0); /* no cursor */
    cbreak();
    keypad(stdscr, TRUE);
    mvprintw(11, 9, "Welcome to tic tac toe!");
    mvprintw(13, 17, "Enjoy!");
    mvprintw(15, 18, "^-^");
    refresh();       //refresh the window after print
    usleep(2000000); //sleep
    erase();         //clear the window
}
void CheckTie()
{
    if (nrOfMoves == 9)
    {
        mvprintw(1, 9, "Tie game!");
        refresh();
        usleep(2000000); //sleep
        mvprintw(1, 9, "          ");
        refresh();
        erase();
        WriteDataTie();
        initBoard(boardData);
        nrOfTieGames++;
        nrOfMoves = 0;
        DrawTicTacToeBoard();
        refresh();
        inputToBeSent = 't';
    }
}
int UpdateTheBoard(int x, int y, int inputPlace, int currentPlayer)
{
    switch (currentPlayer)
    {
    case 1:
        if (CheckValidity(boardData, inputPlace, currentPlayer))
        {
            mvprintw(x, y, "X");
            refresh();
            nrOfMoves++;
            inputToBeSent = inputPlace + '0';
            mvprintw(3, 38, "Nr of moves: %d", nrOfMoves);
            refresh();
            if (CheckWin(boardData))
            {
                winsOfX++;
                nrOfMoves = 0;
                inputToBeSent = 'w';
                initBoard(boardData);
                mvprintw(1, 9, "Player X won!");
                WriteDataX();
                refresh();
                usleep(2000000);
                erase();
                DrawTicTacToeBoard();
            }
            else
            {
                CheckTie();
            }
        }
        break;
        return 1;
    case 0:
        if (CheckValidity(boardData, inputPlace, currentPlayer))
        {
            mvprintw(x, y, "O");
            refresh();
            nrOfMoves++;
            inputToBeSent = inputPlace + '0';
            mvprintw(3, 38, "Nr of moves: %d", nrOfMoves);
            refresh();
            if (CheckWin(boardData))
            {
                winsOfO++;
                nrOfMoves = 0;
                inputToBeSent = 'w';
                initBoard(boardData);
                mvprintw(1, 9, "Player O won!");
                WriteDataO();
                refresh();
                usleep(2000000);
                erase();
                DrawTicTacToeBoard();
            }
            else
            {
                CheckTie();
            }
        }
        break;
        return 1;
    default:
        break;
        return 1;
    }
}

int main(int argc, char *argv[])
{
    uint16_t port = atoi(argv[1]);
    const char *address = "127.0.0.1";
    int sock; /* Socket descriptor */
    int i;    /* counter for data-arguments */

    char message[1];

    if ((sock = CreateTCPClientSocket(address, (unsigned short)port)) == -1) // ip port
    {
        exit(1);
        fprintf(stderr, "ErrorNo: >%d\n", errno);
    }

    while (1)
    {

        unsigned char recvBuffer[1];
        recvBuffer[0] = receiveMessage(sock);

        if (recvBuffer[0] == (unsigned)'1') //player 1 does
        {
            printf("Im Player 1\n");
            initNcurses();
            DrawTicTacToeBoard();
            inputToBeSent = '-'; //reset
            char inputS[1];
            inputToBeReceived = '-'; //reset
            initBoard(boardData);
            mvprintw(12, 50, "You are player X!");
            turn = MY_TURN;

            while (1)
            {
                switch (turn)
                {
                case MY_TURN:
                    mvprintw(16, 50, "MY_TURN");
                    refresh();
                    sleep(1);
                    mvprintw(16, 50, "         ");
                    mvprintw(12, 50, "You are player X!");
                    refresh();
                    input = getch();
                    switch (input)
                    {
                    case '7':

                        UpdateTheBoard(5, 10, 1, INPUT_X);
                        inputS[0] = inputToBeSent;
                        sendMessage(sock, inputS);
                        mvprintw(14, 50, "INPUT 7");
                        refresh();
                        sleep(1);
                        mvprintw(14, 50, "         ");
                        refresh();
                        turn = YOUR_TURN;
                        break;
                    case '8':
                        UpdateTheBoard(5, 18, 2, INPUT_X);
                        inputS[0] = inputToBeSent;
                        sendMessage(sock, inputS);
                        mvprintw(14, 50, "INPUT 8");
                        refresh();
                        sleep(1);
                        mvprintw(14, 50, "         ");
                        refresh();
                        turn = YOUR_TURN;
                        break;
                    case '9':
                        UpdateTheBoard(5, 28, 3, INPUT_X);
                        inputS[0] = inputToBeSent;
                        sendMessage(sock, inputS);
                        mvprintw(14, 50, "INPUT 9");
                        refresh();
                        sleep(1);
                        mvprintw(14, 50, "         ");
                        refresh();
                        turn = YOUR_TURN;
                        break;
                    case '4':
                        UpdateTheBoard(11, 10, 4, INPUT_X);
                        inputS[0] = inputToBeSent;
                        sendMessage(sock, inputS);
                        mvprintw(14, 50, "INPUT 4");
                        refresh();
                        sleep(1);
                        mvprintw(14, 50, "         ");
                        refresh();
                        turn = YOUR_TURN;
                        break;
                    case '5':
                        UpdateTheBoard(11, 18, 5, INPUT_X);
                        inputS[0] = inputToBeSent;
                        sendMessage(sock, inputS);
                        mvprintw(14, 50, "INPUT 5");
                        refresh();
                        sleep(1);
                        mvprintw(14, 50, "         ");
                        refresh();
                        turn = YOUR_TURN;
                        break;
                    case '6':
                        UpdateTheBoard(11, 28, 6, INPUT_X);
                        inputS[0] = inputToBeSent;
                        sendMessage(sock, inputS);
                        mvprintw(14, 50, "INPUT 6");
                        refresh();
                        sleep(1);
                        mvprintw(14, 50, "         ");
                        refresh();
                        turn = YOUR_TURN;
                        break;
                    case '1':
                        UpdateTheBoard(17, 10, 7, INPUT_X);
                        inputS[0] = inputToBeSent;
                        sendMessage(sock, inputS);
                        mvprintw(14, 50, "INPUT 1");
                        refresh();
                        sleep(1);
                        mvprintw(14, 50, "         ");
                        refresh();
                        turn = YOUR_TURN;
                        break;
                    case '2':
                        UpdateTheBoard(17, 18, 8, INPUT_X);
                        inputS[0] = inputToBeSent;
                        sendMessage(sock, inputS);
                        mvprintw(14, 50, "INPUT 2");
                        refresh();
                        sleep(1);
                        mvprintw(14, 50, "         ");
                        refresh();
                        turn = YOUR_TURN;
                        break;
                    case '3':
                        UpdateTheBoard(17, 28, 9, INPUT_X);
                        inputS[0] = inputToBeSent;
                        sendMessage(sock, inputS);
                        mvprintw(14, 50, "INPUT 3");
                        refresh();
                        sleep(1);
                        mvprintw(14, 50, "         ");
                        refresh();
                        turn = YOUR_TURN;
                        break;
                    case 'r':
                        mvprintw(8, 38, "Player X: %d", winsOfX);
                        mvprintw(9, 38, "Player O: %d", winsOfO);
                        mvprintw(10, 38, "Tie: %d", nrOfTieGames);
                        mvprintw(14, 50, "INPUT R");
                        refresh();
                        break;
                    default:
                        break;
                    }
                    break;
                case YOUR_TURN:
                    mvprintw(16, 50, "OPONENTS_TURN");
                    refresh();
                    sleep(2);
                    mvprintw(16, 50, "                 ");
                    refresh();
                    inputToBeReceived = receiveMessage(sock); //get from server
                    switch (inputToBeReceived)
                    {
                    case '1':
                        UpdateTheBoard(5, 10, 1, INPUT_O);
                        mvprintw(15, 50, "RECEIVED 1");
                        turn = MY_TURN;
                        break;
                    case '2':
                        UpdateTheBoard(5, 18, 2, INPUT_O);
                        mvprintw(15, 50, "RECEIVED 2");
                        turn = MY_TURN;
                        break;
                    case '3':
                        UpdateTheBoard(5, 28, 3, INPUT_O);
                        mvprintw(15, 50, "RECEIVED 3");
                        turn = MY_TURN;
                        break;
                    case '4':
                        UpdateTheBoard(11, 10, 4, INPUT_O);
                        mvprintw(15, 50, "RECEIVED 4");
                        turn = MY_TURN;
                        break;
                    case '5':
                        UpdateTheBoard(11, 18, 5, INPUT_O);
                        mvprintw(15, 50, "RECEIVED 5");
                        turn = MY_TURN;
                        break;
                    case '6':
                        UpdateTheBoard(11, 28, 6, INPUT_O);
                        mvprintw(15, 50, "RECEIVED 6");
                        turn = MY_TURN;
                        break;
                    case '7':
                        UpdateTheBoard(17, 10, 7, INPUT_O);
                        mvprintw(15, 50, "RECEIVED 7");
                        turn = MY_TURN;
                        break;
                    case '8':
                        UpdateTheBoard(17, 18, 8, INPUT_O);
                        mvprintw(15, 50, "RECEIVED 8");
                        turn = MY_TURN;
                        break;
                    case '9':
                        UpdateTheBoard(17, 28, 9, INPUT_O);
                        mvprintw(15, 50, "RECEIVED 9");
                        turn = MY_TURN;
                        break;
                    case 'r':
                        mvprintw(8, 38, "Player X: %d", winsOfX);
                        mvprintw(9, 38, "Player O: %d", winsOfO);
                        mvprintw(10, 38, "Tie: %d", nrOfTieGames);
                        mvprintw(15, 50, "RECEIVED r");
                        break;
                    case 'w':
                        winsOfO++;
                        nrOfMoves = 0;
                        initBoard(boardData);
                        mvprintw(1, 9, "Player O won!");
                        refresh();
                        usleep(2000000);
                        erase();
                        DrawTicTacToeBoard();
                        refresh();
                        turn = MY_TURN;
                        break;
                    case 't':

                        nrOfTieGames++;
                        nrOfMoves = 0;
                        initBoard(boardData);
                        mvprintw(1, 9, "Tie game!");
                        refresh();
                        usleep(2000000);
                        erase();
                        DrawTicTacToeBoard();
                        refresh();
                        turn = MY_TURN;

                        break;
                    default:
                        break;
                    }
                default:
                    break;
                }
            }
            endwin();
            printf("Game Finished!\n");
        }
        else if (recvBuffer[0] == (unsigned)'2') //player 2 does
        {
            initNcurses();
            DrawTicTacToeBoard();
            inputToBeSent = '-'; //reset
            char inputS[1];
            inputToBeReceived = '-'; //reset
            initBoard(boardData);
            mvprintw(12, 50, "You are player O!");
            turn = YOUR_TURN;
            while (input != 'f')
            {
                switch (turn)
                {
                case MY_TURN:
                    mvprintw(16, 50, "MY_TURN");
                    refresh();
                    sleep(1);
                    mvprintw(16, 50, "         ");
                    mvprintw(12, 50, "You are player O!");
                    refresh();
                    input = getch();
                    switch (input)
                    {
                    case '7':
                        UpdateTheBoard(5, 10, 1, INPUT_O);
                        inputS[0] = inputToBeSent;
                        sendMessage(sock, inputS);
                        mvprintw(14, 50, "INPUT 7");
                        refresh();
                        sleep(1);
                        mvprintw(14, 50, "         ");
                        refresh();
                        turn = YOUR_TURN;
                        break;
                    case '8':
                        UpdateTheBoard(5, 18, 2, INPUT_O);
                        inputS[0] = inputToBeSent;
                        sendMessage(sock, inputS);
                        mvprintw(14, 50, "INPUT 8");
                        refresh();
                        sleep(1);
                        mvprintw(14, 50, "         ");
                        refresh();
                        turn = YOUR_TURN;
                        break;
                    case '9':
                        UpdateTheBoard(5, 28, 3, INPUT_O);
                        inputS[0] = inputToBeSent;
                        sendMessage(sock, inputS);
                        mvprintw(14, 50, "INPUT 9");
                        refresh();
                        sleep(1);
                        mvprintw(14, 50, "         ");
                        refresh();
                        turn = YOUR_TURN;
                        break;
                    case '4':
                        UpdateTheBoard(11, 10, 4, INPUT_O);
                        inputS[0] = inputToBeSent;
                        sendMessage(sock, inputS);
                        mvprintw(14, 50, "INPUT 4");
                        refresh();
                        sleep(1);
                        mvprintw(14, 50, "         ");
                        refresh();
                        turn = YOUR_TURN;
                        break;
                    case '5':
                        UpdateTheBoard(11, 18, 5, INPUT_O);
                        inputS[0] = inputToBeSent;
                        sendMessage(sock, inputS);
                        mvprintw(14, 50, "INPUT 5");
                        refresh();
                        sleep(1);
                        mvprintw(14, 50, "         ");
                        refresh();
                        turn = YOUR_TURN;
                        break;
                    case '6':
                        UpdateTheBoard(11, 28, 6, INPUT_O);
                        inputS[0] = inputToBeSent;
                        sendMessage(sock, inputS);
                        mvprintw(14, 50, "INPUT 6");
                        refresh();
                        sleep(1);
                        mvprintw(14, 50, "         ");
                        refresh();
                        turn = YOUR_TURN;
                        break;
                    case '1':
                        UpdateTheBoard(17, 10, 7, INPUT_O);
                        inputS[0] = inputToBeSent;
                        sendMessage(sock, inputS);
                        mvprintw(14, 50, "INPUT 1");
                        refresh();
                        sleep(1);
                        mvprintw(14, 50, "         ");
                        refresh();
                        turn = YOUR_TURN;
                        break;
                    case '2':
                        UpdateTheBoard(17, 18, 8, INPUT_O);
                        inputS[0] = inputToBeSent;
                        sendMessage(sock, inputS);
                        mvprintw(14, 50, "INPUT 2");
                        refresh();
                        sleep(1);
                        mvprintw(14, 50, "         ");
                        refresh();
                        turn = YOUR_TURN;
                        break;
                    case '3':
                        UpdateTheBoard(17, 28, 9, INPUT_O);
                        inputS[0] = inputToBeSent;
                        sendMessage(sock, inputS);
                        mvprintw(14, 50, "INPUT 3");
                        refresh();
                        sleep(1);
                        mvprintw(14, 50, "         ");
                        refresh();
                        turn = YOUR_TURN;
                        break;
                    case 'r':
                        mvprintw(8, 38, "Player X: %d", winsOfX);
                        mvprintw(9, 38, "Player O: %d", winsOfO);
                        mvprintw(10, 38, "Tie: %d", nrOfTieGames);
                        break;
                    default:
                        break;
                    }
                    break;
                case YOUR_TURN:
                    mvprintw(16, 50, "OPONENTS_TURN");
                    refresh();
                    sleep(2);
                    mvprintw(16, 50, "                 ");
                    refresh();
                    inputToBeReceived = receiveMessage(sock); //get from server
                    switch (inputToBeReceived)
                    {
                    case '1':
                        UpdateTheBoard(5, 10, 1, INPUT_X);
                        mvprintw(15, 50, "RECEIVED 1");
                        turn = MY_TURN;
                        break;
                    case '2':
                        UpdateTheBoard(5, 18, 2, INPUT_X);
                        mvprintw(15, 50, "RECEIVED 2");
                        turn = MY_TURN;
                        break;
                    case '3':
                        UpdateTheBoard(5, 28, 3, INPUT_X);
                        mvprintw(15, 50, "RECEIVED 3");
                        turn = MY_TURN;
                        break;
                    case '4':
                        UpdateTheBoard(11, 10, 4, INPUT_X);
                        mvprintw(15, 50, "RECEIVED 4");
                        turn = MY_TURN;
                        break;
                    case '5':
                        UpdateTheBoard(11, 18, 5, INPUT_X);
                        mvprintw(15, 50, "RECEIVED 5");
                        turn = MY_TURN;
                        break;
                    case '6':
                        UpdateTheBoard(11, 28, 6, INPUT_X);
                        mvprintw(15, 50, "RECEIVED 6");
                        turn = MY_TURN;
                        break;
                    case '7':
                        UpdateTheBoard(17, 10, 7, INPUT_X);
                        mvprintw(15, 50, "RECEIVED 7");
                        turn = MY_TURN;
                        break;
                    case '8':
                        UpdateTheBoard(17, 18, 8, INPUT_X);
                        mvprintw(15, 50, "RECEIVED 8");
                        turn = MY_TURN;
                        break;
                    case '9':
                        UpdateTheBoard(17, 28, 9, INPUT_X);
                        mvprintw(15, 50, "RECEIVED 9");
                        turn = MY_TURN;
                        break;
                    case 'w':
                        winsOfX++;
                        nrOfMoves = 0;
                        initBoard(boardData);
                        mvprintw(1, 9, "Player X won!");
                        refresh();
                        usleep(2000000);
                        erase();
                        DrawTicTacToeBoard();
                        refresh();
                        turn = MY_TURN;
                        break;
                    case 't':

                        nrOfTieGames++;
                        nrOfMoves = 0;
                        initBoard(boardData);
                        mvprintw(1, 9, "Tie game!");
                        refresh();
                        usleep(2000000);
                        erase();
                        DrawTicTacToeBoard();
                        refresh();
                        turn = MY_TURN;

                        break;
                    default:
                        break;
                    }
                default:
                    break;
                }
            }
            endwin();
        }

        exit(1);
    }

    printf("Game Finished!\n");
    exit(1);
}

unsigned char receiveMessage(int sock)
{
    /* Buffer for echo string */
    int recvMsgSize;
    unsigned char recvBuffer[1];

    if ((recvMsgSize = recv(sock, recvBuffer, 1, 0)) < 0)
    {
        printf("Receive FAILED!\n");
    }
    else
    {
        return recvBuffer[0];
    }
}

void sendMessage(int sock, char *message)
{

    if ((send(sock, message, 1, 0)) < 0)
    {
        printf("Send FAILED!\n");
        fprintf(stderr, "ErrorNo: >%d\n", errno);
    }
}

int CreateTCPClientSocket(const char *servIP, unsigned short port)
{
    int sock; /* Socket descriptor */
    int conn;
    struct sockaddr_in echoServAddr; /* Echo server address */

    printf("Server IP: %s\n", servIP);
    printf("Server Port: %hu\n", port);

    /* Create a reliable, stream socket using TCP */
    if ((sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
    {
        printf("socket() failed\n");
    }
    /* Construct the server address structure */
    memset(&echoServAddr, 0, sizeof(echoServAddr));   /* Zero out structure */
    echoServAddr.sin_family = AF_INET;                /* Internet address family */
    echoServAddr.sin_addr.s_addr = inet_addr(servIP); /* Server IP address */
    echoServAddr.sin_port = htons(port);              /* Server port */
    /* Establish the connection to the echo server */

    conn = connect(sock, (struct sockaddr *)&echoServAddr, sizeof(echoServAddr));
    while (conn == -1)
    {
        printf("connect() failed\n");
        conn = connect(sock, (struct sockaddr *)&echoServAddr, sizeof(echoServAddr));
        sleep(1);
    }
    return (sock);
}
