/* For Standard */
#include <stdio.h>
#include <pthread.h>
#include <time.h>
#include <unistd.h>
#include <ctype.h>      // for isupper() etc.
#include <sys/socket.h> // for send() and recv()
#include <memory.h>     // for memset()
#include <arpa/inet.h>  /* for sockaddr_in and inet_ntoa() */
#include <stdlib.h>
/* For signals */
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
//For share memory
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/fcntl.h>
#include <getopt.h>
/* For Semaphores*/
#include <semaphore.h>

#define RCVBUFSIZE 32 /* Size of receive buffer */
#define MAXPENDING 5  /* Maximum outstanding connection requests */
#define SHM_SIZE 100  /* Size of shared memory */

int flag = 0;
char data[1];
int pid;
int CreateTCPServerSocket(unsigned short port);
int AcceptTCPConnection(int servSock);
void CreateOrOpenSemaphore();
void MakeSem1();
/* For signals */
static bool to_quit = false;
static int delaySeconds = 3;
/* Shared Memory */
static int shm_fd = -1;
char *shm_name = "log";
char *shm_addr;
int PID;
// Semaphores
sem_t *semdes = SEM_FAILED;
char *sem_name = "semaphore_";

//creates the shared memory
static char *my_shm_create(char *shm_name, int size)
{
    int rtnval;
    char *shm_addr;
    shm_fd = shm_open(shm_name, O_CREAT | O_EXCL | O_RDWR, 0600);
    //by deafult the shm_fd is -1, so if it didn´t change something went wrong
    if (shm_fd == -1)
    {
        perror("ERROR: shm_open() failed");
    }
    rtnval = ftruncate(shm_fd, size);
    if (rtnval != 0)
    {
        perror("ERROR: ftruncate() failed");
    }
    shm_addr = (char *)mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_SHARED, shm_fd, 0);
    if (shm_addr == MAP_FAILED)
    {
        perror("ERROR: mmap() failed");
    }
    return (shm_addr);
}
/* Signals */
static void my_sig_handler(int signum)
{
    printf("\n signal %d received\n", signum);
}

static void install_sigaction(int signo)
{
    struct sigaction new_action;
    struct sigaction old_action;

    sigemptyset(&new_action.sa_mask);
    new_action.sa_handler = my_sig_handler;
    new_action.sa_flags = SA_RESTART;

    if (sigaction(signo, &new_action, NULL) == -1)
    {
        perror("sigaction(set-my-sig)");
    }
}

void SendSignal(int signal, int target_pid)
{
    install_sigaction(signal);
    int i = 0;

    if (kill(target_pid, signal) != 0)
    {
        perror("kill()");
    }
    to_quit = true;

    printf("Signal: sent to %d\n\n", target_pid);
}

void *
thread1Func(void *arg)
{
    int clntSock = (int64_t)arg;
    int recvMsgSize;    /* Size of received message */
    char recvBuffer[1]; /* Buffer for echo string */
    char message[1];
    int state = 1;

    sleep(1);
    message[0] = '1';
    send(clntSock, message, 1, 0);
    CreateOrOpenSemaphore();
    MakeSem1();

    while (1)
    {
        sem_wait(semdes);
        if (state == 1)
        {
            if ((recvMsgSize = recv(clntSock, recvBuffer, 1, 0)) < 0)
            {
                printf("receive1 FAILED!\n");
            }
            else
            {
                printf("1Received: %c\n", recvBuffer[0]);
                data[0] = recvBuffer[0];
            }
            state++;
        }
        else
        {
            send(clntSock, data, 1, 0);

            if ((recvMsgSize = recv(clntSock, recvBuffer, 1, 0)) < 0)
            {
                printf("receive1 FAILED!\n");
            }
            else
            {
                printf("1Received: %c\n", recvBuffer[0]);
                data[0] = recvBuffer[0];
                if (recvBuffer[0] == 'w' || recvBuffer[0] == 't')
                {
                    SendSignal(4, pid);
                }
            }
        }
        sem_post(semdes);
        sleep(1);
    }
    return NULL;
}

void *logThreadFunction(void *arg)
{
    int clntSock = (int64_t)arg;
    int recvMsgSize;             /* Size of received message */
    unsigned char recvBuffer[4]; /* Buffer for echo string */
    char message[1];
    int received_int = 0;

    sleep(1);
    message[0] = '3';
    send(clntSock, message, 1, 0);
    printf("Please Enter the PID of the LOG process: \n");
    scanf("%d\n", &pid);
}

void *thread2Func(void *arg)
{
    int clntSock = (int64_t)arg;
    int recvMsgSize;             /* Size of received message */
    unsigned char recvBuffer[1]; /* Buffer for echo string */
    char message[1];

    sleep(1);
    message[0] = '2';
    send(clntSock, message, 1, 0);
    CreateOrOpenSemaphore();

    while (1)
    {
        sem_wait(semdes);

        send(clntSock, data, 1, 0);

        if ((recvMsgSize = recv(clntSock, recvBuffer, 1, 0)) < 0)
        {
            printf("2Receive FAILED!\n");
        }
        else
        {
            printf("2Received: %c\n", recvBuffer[0]);
            data[0] = recvBuffer[0];
            if (recvBuffer[0] == 'w' || recvBuffer[0] == 't')
            {
                SendSignal(4, pid);
            }
        }
        sem_post(semdes);
        sleep(1);
    }
    return NULL;
}

int main(int argc, char *argv[])
{
    int servSock;     /* Socket descriptor for server */
    int64_t clntSock; /* Socket descriptor for server */
    uint16_t port = atoi(argv[1]);
    int oneClient = 0;
    pthread_t thread1; /* Thread ID from pthread_create() */
    pthread_t thread2; /* Thread ID from pthread_create() */
    pthread_t threadLog;
    int recvMsgSize;             /* Size of received message */
    unsigned char recvBuffer[1]; /* Buffer for echo string */

    servSock = CreateTCPServerSocket((unsigned short)port);

    printf("Sucessfully created the Server Socket: %d  With a port: %hu\n", servSock, (unsigned short)port);
    /* Create the shared memory */
    shm_addr = my_shm_create(shm_name, SHM_SIZE);

    clntSock = AcceptTCPConnection(servSock); // accept connections
    pthread_create(&thread1, NULL, thread1Func, (void *)clntSock);

    clntSock = AcceptTCPConnection(servSock); // accept connections
    pthread_create(&thread2, NULL, thread2Func, (void *)clntSock);
    
    clntSock = AcceptTCPConnection(servSock); // accept connections
    pthread_create(&threadLog, NULL, logThreadFunction, (void *)clntSock);

    pthread_join(thread1, NULL);
    pthread_join(thread2, NULL);
    pthread_join(threadLog, NULL);
}

void CreateOrOpenSemaphore()
{
    semdes = sem_open(sem_name, O_CREAT | O_EXCL, 0600, 0);
    int rtnVal;

    if (semdes == SEM_FAILED)
    {
        // perror("ERROR: sem_Create failed\n");
        semdes = sem_open(sem_name, 0);

        if (semdes == SEM_FAILED)
        {
            perror("ERROR: sem_open() failed\n");
        }
    }

    sem_getvalue(semdes, &rtnVal);
}

void MakeSem1()
{
    int rtnVal;

    while (rtnVal != 1)
    {
        if (rtnVal <= 0)
        {
            sem_post(semdes);
        }
        else if (rtnVal > 0)
        {
            sem_wait(semdes);
        }
        sem_getvalue(semdes, &rtnVal);
    }
}

int CreateTCPServerSocket(unsigned short port)
{
    int sock;                        /* socket to create */
    struct sockaddr_in echoServAddr; /* Local address */
    int bin;
    int list;

    /* Create socket for incoming connections */
    if ((sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
    {
        printf("socket() failed\n");
        exit(1);
    }

    /* Construct local address structure */
    memset(&echoServAddr, 0, sizeof(echoServAddr));   /* Zero out structure */
    echoServAddr.sin_family = AF_INET;                /* Internet address family */
    echoServAddr.sin_addr.s_addr = htonl(INADDR_ANY); /* Any incoming interface */
    echoServAddr.sin_port = htons(port);              /* Local port */

    /* Bind to the local address */
    if (bin = (bind(sock, (struct sockaddr *)&echoServAddr, sizeof(echoServAddr))) < 0)
    {
        printf("bind() failed\n");
        exit(1);
    }

    /* Mark the socket so it will listen for incoming connections */
    if (list = (listen(sock, MAXPENDING)) < 0)
    {
        printf("listen() failed\n");
        exit(1);
    }

    return (sock);
}

int AcceptTCPConnection(int servSock)
{
    // 'servSock' is obtained from CreateTCPServerSocket()
    int clntSock;                    /* Socket descriptor for client */
    struct sockaddr_in echoClntAddr; /* Client address */
    unsigned int clntLen;            /* Length of client address data structure */

    /* Set the size of the in-out parameter */
    clntLen = sizeof(echoClntAddr);

    /* Wait for a client to connect */
    clntSock = accept(servSock, (struct sockaddr *)&echoClntAddr, &clntLen);
    if (clntSock < 0)
    {
        printf("accept() failed\n");
        exit(1);
    }
    
    return (clntSock);
}