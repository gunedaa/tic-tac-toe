#include <stdio.h>
#include <pthread.h>
#include <time.h>
#include <unistd.h>

#include <ctype.h>      // for isupper() etc.
#include <sys/socket.h> // for send() and recv()

#include <memory.h>    // for memset()
#include <arpa/inet.h> /* for sockaddr_in and inet_ntoa() */
#include <stdlib.h>

int flag = 0;
char *data = "noone touched!";

int CreateTCPServerSocket(unsigned short port);
int AcceptTCPConnection(int servSock);

void *thread1Func(void *arg)
{
    printf("Hello Im thread 1!\n");
    while (1)
    {
        sleep(1);
        if (flag == 0)
        {
            printf("1:My turn!\n");
            flag = 1;
        }
        else
        {
            printf("1:Waiting!\n");
        }
    }
    return NULL;
}

void *thread2Func(void *arg)
{
    printf("Hello Im thread 2!\n");
    while (1)
    {
        sleep(1);
        if (flag == 1)
        {
            printf("2:My turn!\n");
            flag = 0;
        }
        else
        {
            printf("2:Waiting!\n");
        }
    }
    return NULL;
}

int main(int argc, char *argv[])
{
    int servSock; /* Socket descriptor for server */
    int clntSock; /* Socket descriptor for server */
    int oneClient = 0;
    pthread_t thread1; /* Thread ID from pthread_create() */
    pthread_t thread2; /* Thread ID from pthread_create() */

    servSock = CreateTCPServerSocket((uint64_t)argv[1]);

    // pthread_create(&thread1, NULL, thread1Func, NULL);
    // pthread_create(&thread2, NULL, thread2Func, NULL);

    while (oneClient != 2)
    {
        clntSock = AcceptTCPConnection(servSock);
        printf("1\n");
        // pthread_create(&thread1, NULL, thread1Func, NULL);
    }

    pthread_join(thread1, NULL);
}

int CreateTCPServerSocket(unsigned short port)
{
    int sock;                        /* socket to create */
    struct sockaddr_in echoServAddr; /* Local address */

    /* Create socket for incoming connections */
    if ((sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
    {
        printf("socket() failed\n");
        exit(1);
    }
    /* Construct local address structure */
    memset(&echoServAddr, 0, sizeof(echoServAddr));   /* Zero out structure */
    echoServAddr.sin_family = AF_INET;                /* Internet address family */
    echoServAddr.sin_addr.s_addr = htonl(INADDR_ANY); /* Any incoming interface */
    echoServAddr.sin_port = htons(port);              /* Local port */
    /* Bind to the local address */
    if (bind(sock, (struct sockaddr *)&echoServAddr, sizeof(echoServAddr)) < 0)
    {
        printf("bind() failed\n");
        exit(1);
    }
    /* Mark the socket so it will listen for incoming connections */
    if (listen(sock, 5) < 0)
    {
        printf("listen() failed\n");
        exit(1);
    }
    return (sock);
}

int AcceptTCPConnection(int servSock)
{
    // 'servSock' is obtained from CreateTCPServerSocket()
    int clntSock;                    /* Socket descriptor for client */
    struct sockaddr_in echoClntAddr; /* Client address */
    unsigned int clntLen;            /* Length of client address data structure */
    /* Set the size of the in-out parameter */
    clntLen = sizeof(echoClntAddr);
    /* Wait for a client to connect */
    clntSock = accept(servSock, (struct sockaddr *)&echoClntAddr, &clntLen);
    if (clntSock < 0)
    {
        printf("accept() failed\n");
        exit(1);
    }
    /* clntSock is connected to a client! */
    return (clntSock);
}