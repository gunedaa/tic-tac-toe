#ifndef _CLIENTSIDELOG_H
#define _CLIENTSIDELOG_H

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/fcntl.h>
#include <stdlib.h>
#include <getopt.h>

#define SHM_SIZE 100
#define MAX_DATA 80
static int  shm_fd = -1;
char *shm_name = "log";
char *shm_addr;

static char * my_shm_create (char * shm_name, int size);
static char * my_shm_open (char * shm_name);
int closure(const char* name, const size_t size, void *addr);
void writeData(void* addr, char* result);

#endif