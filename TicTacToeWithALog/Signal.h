#ifndef _SIGNAL_H
#define _SIGNAL_H
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>
#include <time.h>

static bool		to_quit = false;
static int		delaySeconds = 3;

static void my_sig_handler (int signum);
static void install_sigaction (int signo);
void SendSignal(int signal, int target_pid);
int ReceiveSignal(int signal);

#endif
