#include <stdio.h>
#include <string.h>     // for strlen()
#include <stdlib.h>     // for exit()
#include <sys/socket.h> // for send() and recv()
#include <unistd.h>     // for sleep(), close()

#include <arpa/inet.h> // for sockaddr_in and inet_ntoa()
#include <pthread.h>   // for POSIX threads
#include <time.h>      // for time()
#include <errno.h>
/* For signal */
#include <stdbool.h>
#include <signal.h>
#include <time.h>
/* Shared memory */
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/fcntl.h>
#include <getopt.h>

#include <sys/ipc.h> 
#include <sys/shm.h> 

static bool to_quit = false;
static int delaySeconds = 3;

#define SHM_SIZE 100
#define MAX_DATA 80
static int shm_fd = -1;
char *shm_name = "log";
char *shm_addr;

#define RCVBUFSIZE 32 /* Size of receive buffer */
#define SHM_SIZE 100  /* Size of shared memory */
#define MAX_DATA 80

/* Method signatures */
int CreateTCPClientSocket(const char *servIP, unsigned short port);
unsigned char receiveMessage(int sock);
void sendMessage(int sock, char *message);
static char *my_shm_open(char *shm_name);
int closure(const char *name, const size_t size, void *addr);
static void my_sig_handler(int signum);
static void install_sigaction(int signo);
int ReceiveSignal(int signal);

int main(int argc, char *argv[])
{
    uint16_t port = atoi(argv[1]);
    const char *address = "127.0.0.1";
    int sock;                    /* Socket descriptor */
    int i;                       /* counter for data-arguments */
    int serverSocket;
    
    int PID = getpid();

    if ((sock = CreateTCPClientSocket(address, (unsigned short)port)) == -1) // ip port
    {
        exit(1);
        fprintf(stderr, "ErrorNo: >%d\n", errno);
    }

    unsigned char recvBuffer[1];
    recvBuffer[0] = receiveMessage(sock);
    // printf("recvBuffer: %u\n", recvBuffer[0]);
    if (recvBuffer[0] == (unsigned)'3')
    {
        printf("PID: %d\n", PID);
        //sendMessage(sock,message);
    }
    while (true)
    {
        printf("busy waiting\n");
        ReceiveSignal(4);
    }
    return 1;
}

/* open and read from the shared memory by passing its name */
static char *my_shm_open(char *shm_name)   
{
    int size;
    char *shm_addr;

    printf("Calling shm_open('%s')\n", shm_name);

    //opens the memory file with the matching name
    shm_fd = shm_open(shm_name, O_RDWR, 0600); // O_RDWR -> open read write (mode)
    if (shm_fd == -1)
    {
        perror("ERROR: open failed");
    }
    
    size = lseek(shm_fd, 0, SEEK_END);
    shm_addr = (char *)mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_SHARED, shm_fd, 0);
    printf("%s", (char*)shm_addr); //read the shm and print
    if (shm_addr == MAP_FAILED)
    {
        perror("ERROR: mmap() failed");
    }
    printf("mmap() returned %p\n", shm_addr);

    return (shm_addr);
}
int closure(const char *name, const size_t size, void *addr)
{
    int ret;
    ret = munmap(addr, size);
    if (ret == -1)
    {
        perror("munmap");
    }
}


/* Signals */
static void my_sig_handler(int signum)
{
    printf("\n signal %d received\n", signum);
    printf("Start reading the memory...\n");
    shm_addr = my_shm_open(shm_name);
    usleep(20000);
}

static void install_sigaction(int signo)
{
    struct sigaction new_action;
    struct sigaction old_action;

    sigemptyset(&new_action.sa_mask);
    new_action.sa_handler = my_sig_handler;
    new_action.sa_flags = SA_RESTART;

    if (sigaction(signo, &new_action, NULL) == -1)
    {
        perror("sigaction(set-my-sig)");
    }
}

int ReceiveSignal(int signal)
{
    int i = 0;
    install_sigaction(signal);

    while (to_quit == false)
    {
        sleep(delaySeconds);
        //printf("Log:iteration %d\n", i);
        i++;
    }
    printf("Signal: new input!\n\n");
    return 1;
}
unsigned char receiveMessage(int sock)
{
    /* Buffer for echo string */
    int recvMsgSize;
    unsigned char recvBuffer[1];

    if ((recvMsgSize = recv(sock, recvBuffer, 1, 0)) < 0)
    {
        printf("Receive FAILED!\n");
    }
    else
    {

        // printf("Received:: %s\n", recvBuffer);
        return recvBuffer[0];
    }
}

void sendMessage(int sock, char *message)
{

    if ((send(sock, message, 1, 0)) < 0)
    {
        printf("Send FAILED!\n");
        fprintf(stderr, "ErrorNo: >%d\n", errno);
    }
}

int CreateTCPClientSocket(const char *servIP, unsigned short port)
{
    int sock; /* Socket descriptor */
    int conn;
    struct sockaddr_in echoServAddr; /* Echo server address */

    printf("Server IP: %s\n", servIP);
    printf("Server Port: %hu\n", port);

    /* Create a reliable, stream socket using TCP */
    if ((sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
    {
        printf("socket() failed\n");
    }
    /* Construct the server address structure */
    memset(&echoServAddr, 0, sizeof(echoServAddr));   /* Zero out structure */
    echoServAddr.sin_family = AF_INET;                /* Internet address family */
    echoServAddr.sin_addr.s_addr = inet_addr(servIP); /* Server IP address */
    echoServAddr.sin_port = htons(port);              /* Server port */
    /* Establish the connection to the echo server */

    conn = connect(sock, (struct sockaddr *)&echoServAddr, sizeof(echoServAddr));
    while (conn == -1)
    {
        printf("connect() failed\n");
        conn = connect(sock, (struct sockaddr *)&echoServAddr, sizeof(echoServAddr));
        sleep(1);
    }
    return (sock);
}