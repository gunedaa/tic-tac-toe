/* This code enables to send singals to Log from 
    Tic Tac Toe Game Server and the Log will receive these signals.
 */
#include "Signal.h"

static void my_sig_handler (int signum)
{
	printf ("\n signal %d received\n", signum);
}

static void install_sigaction (int signo)
{
    struct sigaction 	new_action;
    struct sigaction	old_action;

    printf ("install signal handler for signal %d\n", signo);
    printf("PID = %i\n", getpid());
    sigemptyset (&new_action.sa_mask);
    new_action.sa_handler = my_sig_handler;
    new_action.sa_flags   = SA_RESTART;

    if (sigaction (signo, &new_action, NULL) == -1)
    {
        perror ("sigaction(set-my-sig)");
    }
}

int ReceiveSignal(int signal){
	int		i = 0;
    install_sigaction (signal);
    
    while (to_quit == false)
    {
		sleep (delaySeconds);
		printf ("Signal:iteration %d\n", i);
		i++;
    }
    printf ("Signal: new input!\n\n");
    return 1;
}