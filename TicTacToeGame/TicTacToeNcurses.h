#ifndef _TICTACTOENCURSES_H
#define _TICTACTOENCURSES_H

#include <stdint.h>
#include <stdio.h>
#include <unistd.h>
#include <ncurses.h>
#include <curses.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>

const int INPUT_X = 1;
const int INPUT_O = 0;

typedef enum {
    X = 1,
    O = 0
}Player;

int boardData[9]; //store the values for each square
Player player = X;
char input; // store player input
int nrOfMoves = 0; // store the nr of moves (9 at most)
int winsOfX = 0;  // store the wins of X
int winsOfO = 0; // store the wins of O
int nrOfTieGames = 0; // store the nr of times the game was a tie

//Functions
void initBoard(int * boardData);
void DrawTicTacToeBoard();
bool CheckValidity(int * boardData, int place, int XorO);
bool CheckWin(int * boardData);
void initNcurses();
void CheckTie();
int UpdateTheBoard(int x, int y, int inputPlace, int currentPlayer);

#endif
 